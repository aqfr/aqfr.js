const Aqfr = require('..').Aqfr;
const axios = require('axios');


async function testNotify() {
    const aqfr1 = new Aqfr();
    const aqfr2 = new Aqfr();
    const addresses = {};
    await aqfr2.connectWs({});
    const xrpAddress2 = await aqfr2.initAccount({});
    addresses.central = xrpAddress2;


    await aqfr1.connectWs({});
    await aqfr1.initAccount({});

    const aqfr3 = new Aqfr();
    await aqfr3.connectWs({});

    aqfr3.subscribe(function (notification) {
        console.log('@===>' + JSON.stringify(notification));
    }, addresses.central);

    aqfr3.on('received-verified-notification', function(notification) {
        console.log("EVENT RECEIVED1:" + JSON.stringify(notification));
    });

    aqfr3.on('received-non-verified-notification', function(notification) {
        console.log("EVENT RECEIVED2:" + JSON.stringify(notification));
    });

    aqfr3.on('received-non-verified-notification', function(notification) {
        console.log("EVENT RECEIVED2:" + JSON.stringify(notification));
    });


    await aqfr1.rotateKeys( addresses.central );
    await aqfr1.rotateKeys( addresses.central );
    await aqfr1.rotateKeys( addresses.central );
    await aqfr1.signSaveAndNotify('tweet', {'tweet': 'my tweet2'},
        addresses.central);
    await aqfr1.rotateKeys( addresses.central );
    await aqfr1.signSaveAndNotify('tweet', {'tweet': 'my tweet3'},
        addresses.central);
    await aqfr1.rotateKeys( addresses.central );
    await aqfr1.signSaveAndNotify('tweet', {'tweet': 'my tweet4'},
        addresses.central);
    await aqfr1.rotateKeys( addresses.central );
    await aqfr1.signSaveAndNotify('tweet', {'tweet': 'my tweet5'},
        addresses.central);
    await aqfr1.rotateKeys( addresses.central );


    // this is to show how we can pickup where another aqfr left off.
    const aqfr4 = new Aqfr();
    await aqfr4.connectWs({});
    setTimeout(function () {
        aqfr4.initAccount(
            {'xrpAddress': aqfr1.xrpAddress, 'secret': aqfr1.secret}).then(
            function () {
                aqfr4.jwksStore = aqfr1.keyStore;
                aqfr4.signSaveAndNotify('tweet', {'tweet': 'my tweet6'},
                    addresses.central).then(function () {
                    console.log('tweeted 6');
                });
            });
    }, 5000);
}

async function testExternal() {
    const aqfr1 = new Aqfr();
    await aqfr1.connectWs({});
    const xrpAddress1 = await aqfr1.initAccount({});
    await aqfr1.rotateKeys();

    const aqfr2 = new Aqfr();
    await aqfr2.connectWs({});

    const xrpAddress2 = await aqfr2.initAccount({});
    await aqfr2.rotateKeys();

    aqfr2.generateSignedAndEncryptedMessage( { tweet : 'hi'}, xrpAddress1 ).then(function( msg ) {
        aqfr1.handleSignedAndEncryptedMessage( msg ).then( function( handled ) {
            console.log( JSON.stringify( handled ));
        }) ;
    });

}




function testAccountGeneration( aqfr ) {
    const addressAndSecret = aqfr.generateNewXrpAddress();
    console.log('is valid address:' + aqfr.isValidXrpAddress( addressAndSecret.address));
    console.log('is valid secret:' + aqfr.isValidXrpAddressSecret( addressAndSecret.secret));
}

function iterateTransactions( xrpAddress ) {
    return new Promise(function(resolve, reject) {
        const allTxns = [];
        aqfr.getAccountTxns('rPDQ1YbjwBWKShoGyYorkhg7L3St7Bnigp', 100, function(txns) {
            console.log('txns:' + JSON.stringify(txns, null, 2));

        });

    });
}

function getMore(aqfr, account, marker ) {
    aqfr.getAccountTxnsWithMarker(account, 200, marker ).then(function(txns) {
        handleTxns( txns.transactions );
        if (txns.transactions.length === 200) {
            getMore(aqfr, account, txns.marker );
        }
        else {
            for (var idx in newAccounts) {
                startTxns( aqfr, newAccounts[idx] );
            }
        }
    });
}
const yo = 'rfGDefKkiNojmMV7BhBRFGfRk6XPHYpMYo';

const newAccounts = [];
let rootPaymentAmount = 0;
let rootPaymentCount = 0;
let nonRootTxns = 0;

function doPrint() {
    console.log('PRINT COUNT:' + ' ' + newAccounts.length+ ', TotalPaymentAmount: ' + (rootPaymentAmount/1000000) +
        ', TotalPayments:' + rootPaymentCount + ', TotalTxns:' + nonRootTxns);
}

function getMoreNonRoot(aqfr, account, marker ) {
    aqfr.getAccountTxnsWithMarker(account, 200, marker ).then(function(txns) {
        handleNonRoot( txns.transactions );
        doPrint();
        if (txns.transactions.length === 200) {
            getMoreNonRoot(aqfr, account, txns.marker );
        }

    });
}

function handleTxns( txns ) {
    for (var idx in txns) {
        const txn = txns[idx];
        if (txn.tx.TransactionType === 'Payment') {
            if (txn.tx.Account === yo) {

                newAccounts.push(txn.tx.Destination);

            }
            rootPaymentCount += 1;
            rootPaymentAmount += parseInt(txn.tx.Amount);
        }
    }
    nonRootTxns += txns.length;

}

function handleNonRoot( txns ) {
    for (var idx in txns) {
        const txn = txns[idx];
        if (txn.tx.TransactionType === 'Payment') {
            rootPaymentAmount += parseInt(txn.tx.Amount);
            rootPaymentCount += 1;
        }
    }
    nonRootTxns += txns.length;
}

function startTxns( aqfr, account ) {
    aqfr.getAccountTxns(account, 200).then(function(txns) {
        handleNonRoot( txns.transactions );

        if (txns.transactions.length === 200) {
            getMoreNonRoot(aqfr, account, txns.marker);
        }

    });
}
/*
initAqfrWithKeys().then(function(aqfr) {
    aqfr.getAccountTxns('rfGDefKkiNojmMV7BhBRFGfRk6XPHYpMYo', 200).then(function(txns) {
        console.log('Recieved:' + txns.transactions.length);
        handleTxns( txns.transactions );

        if (txns.transactions.length === 200) {
            getMore(aqfr, 'rfGDefKkiNojmMV7BhBRFGfRk6XPHYpMYo', txns.marker , 200);
        }
        else {

            for (var idx in newAccounts) {
                startTxns( aqfr, newAccounts[idx] );
                console.log('TMP COUNT:' + ' ' + newAccounts.length+ ', TotalPaymentAmount: ' + (rootPaymentAmount/1000000) +
                    ', TotalPayments:' + rootPaymentCount + ', TotalTxns:' + nonRootTxns);
            }

            console.log('FINAL COUNT:' + ' ' + newAccounts.length+ ', TotalPaymentAmount: ' + (rootPaymentAmount/1000000) +
                ', TotalPayments:' + rootPaymentCount + ', TotalTxns:' + nonRootTxns);
        }
    });
})*/

async function initAqfrWithKeys() {
    const central = await axios.get('https://api.aqfr.network/v1/bootstrap');
    const aqfr = new Aqfr();

    return new Promise(function(resolve) {

        aqfr.connectWs({server: 'wss://s1.ripple.com/'}).then(function(connected) {
            aqfr.initAccount({xrpAddress: 'rYP8LoefLVbhfUD1AZzo6dWXWtCa6zm7h',
                secret: ' '}).then(function(inited) {
                const txns = [];
                aqfr.on('transaction-received', function(notification) {
                    console.log("EVENT RECEIVED2:" + JSON.stringify(notification));
                    txns.push(notification.txn);
                });
                aqfr.subscribe(function(notification) {
                    console.log('notification aqfr1 ===>:' + JSON.stringify(notification));
                } );
                resolve(aqfr);
            });
        });
    })
}

function createEscrow() {
    initAqfrWithKeys().then(function(aqfr) {
        console.log('aqfr inited:'+ aqfr.xrpAddress);
        const cryptoCondition = aqfr.createCryptoCondition();
        /*
             aqfr.createEscrowWithExpiration("10000", cryptoCondition.condition,
                aqfr.jsDateToRippleSeconds(new Date())+180,
                'rMwXJutPuyiNXXMuJCrLGfXVLzfhMFZXWA').then(function(txn) {
                    console.log('txn:' + JSON.stringify(txn, null, 2));
                    aqfr.waitUntilFinalHash(txn.tx_json.hash).then(function(finished){
                        console.log('finished:' + JSON.stringify(finished));

                        aqfr.finishEscrow(txn.tx_json.hash, cryptoCondition.fulfillment).then(function(ok) {
                            console.log('finished escrow:' + JSON.stringify(ok, null, 2));
                            aqfr.waitUntilFinalHash(ok.tx_json.hash).then(function(finished) {
                                console.log('FINISHED FINISHED:' + JSON.stringify(finished, null, 2));
                            }).catch(function(error) {
                                console.log("NOT FINISHED FINISHED:" + JSON.stringify( error, null, 2));
                            })
                        }).catch(function(error) {
                            console.log("NOT FINISH SUBMIT:" + error);
                        })
                    })
            }).catch(function(error) {
                console.log('Error:' + JSON.stringify(error, null, 2));
            })*/

        aqfr.getEscrow('97A1D4E469D6DDF82A4D6CCA2F1155E91DD03DA92223ABC0010E50E6AA889248').then(function(txn) {
            console.log('cancel escrow ok:' + JSON.stringify(txn, null, 2));
            /* aqfr.waitUntilFinalHash(txn.tx_json.hash).then(function(finished){
                 console.log('finished:' + JSON.stringify(finished));
             })*/
        }).catch(function(error) {
            console.log('cancel escrow failed:' +  JSON.stringify(error, null, 2) );
        })
        console.log(aqfr.rippleSecondsToJsDate(632580871));
    })
}


function cancelEscrow() {
    initAqfrWithKeys().then(function(aqfr) {
        console.log('aqfr inited:'+ aqfr.xrpAddress);

        aqfr.cancelEscrow('97A1D4E469D6DDF82A4D6CCA2F1155E91DD03DA92223ABC0010E50E6AA889248').then(function(txn) {
            console.log('cancel escrow ok:' + JSON.stringify(txn, null, 2));
            aqfr.waitUntilFinalHash(txn.tx_json.hash).then(function(finished){
                 console.log('finished:' + JSON.stringify(finished));
             })
        }).catch(function(error) {
            console.log('cancel escrow failed:' +  JSON.stringify(error, null, 2) );
        })
        console.log(aqfr.rippleSecondsToJsDate(632580871));
    })
}



function getEscrow() {
    initAqfrWithKeys().then(function(aqfr) {
        console.log('aqfr inited:'+ aqfr.xrpAddress);

        aqfr.getEscrow('97A1D4E469D6DDF82A4D6CCA2F1155E91DD03DA92223ABC0010E50E6AA889248').then(function(txn) {
            console.log('got escrow ok:' + JSON.stringify(txn, null, 2));

        }).catch(function(error) {
            console.log('cancel escrow failed:' +  JSON.stringify(error, null, 2) );
        })
        console.log(aqfr.rippleSecondsToJsDate(632580871));
    })
}

getEscrow();