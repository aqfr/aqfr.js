'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const RippleAPI = require('ripple-lib').RippleAPI;
const axios = require('axios');
const WebSocket = require('isomorphic-ws');
const events = require('events');
const nacl = require('tweetnacl');
const decodeUTF8 = require('tweetnacl-util').decodeUTF8;
const encodeUTF8 = require('tweetnacl-util').encodeUTF8;
const encodeBase64  = require('tweetnacl-util').encodeBase64;
const decodeBase64 = require('tweetnacl-util').decodeBase64;
const crypto = require('crypto');
const cc = require('five-bells-condition');

const VERSION = '0.0.79';

class Aqfr extends events.EventEmitter {

    constructor() {
        super();
        this.api = null;
        this.xrpAddress = null;
        this.secret = null;
        this.connectedApi = false;
        this.mode = null;
        this.ws = null;
        this.requestMap = {};
        this.subscribeCallback = null;
        this.dataCallback = null;
        this.network = null;
        this.id = 0;
        this.sequence = 0;
        this.keyStore = {};
        this.wsOptions = null;
        this.pingTimeout = null;
        this.monitorConnection();
        this.balance = { balance : "PENDING" };
        this.keyStoreListener = null;
        console.log('Aqfr.js initialized.  Version=' + VERSION);
    }

    heartbeat() {
         
        if (this.pingTimeout != null) {
            clearTimeout(this.pingTimeout);
        }
        const aqfr = this;

        this.pingTimeout = setTimeout(() => {
            console.log('ping timed out');
            aqfr.emit('aqfr-ledger-ping', { state: 'timeout'});
            if (aqfr.ws != null) {
                aqfr.connectedApi = false;
            }
        }, 5000);

        if (this.xrpAddress !== null) {
            console.log('Heartbeat: -> ping account info');
            this.promiseWsAccountInfo(this.xrpAddress).then(function(response) {
                console.log('Heartbeat: <- ping account info');
                aqfr.emit('aqfr-ledger-ping', { state: 'ok'});
                aqfr.connectedApi = true;
                if (aqfr.pingTimeout != null) {
                    clearTimeout(aqfr.pingTimeout);
                }
                setTimeout(function() {
                    aqfr.heartbeat();
                }, 30000);
            });
        }
        else {
            console.log('Heartbeat: -> ping');
            this.ping().then(function(response) {
                aqfr.emit('aqfr-ledger-ping', { state: 'ok'});
                console.log('Heartbeat: <- ping');
                aqfr.connectedApi = true;
                if (aqfr.pingTimeout != null) {
                    clearTimeout(aqfr.pingTimeout);
                }
                setTimeout(function() {
                    aqfr.heartbeat();
                }, 30000);
            });
        }
    }

    monitorConnection() {
        const aqfr = this;
        setInterval(function() {
            if (aqfr.ws == null && aqfr.wsOptions != null) {
                aqfr.connectedApi = false;
                aqfr.connectWs(aqfr.wsOptions).then(function (connectionResult) {
                    console.log("Monitor Connection: reconnection result:" + connectionResult);
                });
            }
            else if (aqfr.wsOptions == null) {
                console.log("Monitor Connection: no ws options, not reconnecting");
            }
            else if (aqfr.connectedApi) {
                console.log("Monitor Connection: already connected, not reconnecting:" + aqfr.ws.readyState);

            }
        }, 60000);
    }

    async saveAndNotify( key, data,
        notificationAddress ) {
        const aqfr = this;
        return new Promise( async function(resolve, reject) {
            const memoResponse1 = await aqfr.setMemo(key, data);
            const domainResponse2 = await aqfr.setDomain(
                'xrp://' + memoResponse1.tx_json.hash );
            if (typeof notificationAddress !== 'undefined' && notificationAddress != null) {
                await aqfr.pay(notificationAddress, "20",
                    'notification',
                    {hash: 'xrp://' + memoResponse1.tx_json.hash});
            }

            const aqfrRecord = {
                pointer: 'xrp://' + domainResponse2.tx_json.hash,
                record: 'xrp://' + memoResponse1.tx_json.hash
            };

            setTimeout(() => {
                aqfr.waitUntilFinalHash(domainResponse2.tx_json.hash).then(function(domainHash) {
                    console.log('gotDomainHash:' + domainHash);
                    aqfr.waitUntilFinalHash(memoResponse1.tx_json.hash).then(function(memoHash) {
                        console.log('got Memo hash:' + memoHash );
                        axios.post('https://api.aqfr.network/v1/aqfr', aqfrRecord).then(function(response) {
                            console.log('Aqfr Record Posted.  Pointer = ' + aqfrRecord.pointer + ', Record = ' + aqfrRecord.record );
                        }).catch(function(error) {
                            console.log('Aqfr Record Not Posted Error: ' + error );
                        });


                    });
                });
            }, 500);

            resolve( 'xrp://' +memoResponse1.tx_json.hash) ;

        });



    }

    async generateSignedAndEncryptedMessage( msg, destinationAddress ) {
        const aqfr = this;
        return new Promise(function (resolve, reject) {
            aqfr.getFirstRecordByType(destinationAddress, 'publickeys').then(function(theirJwkRecord) {
                if (typeof theirJwkRecord.hash === 'undefined') {
                    console.log("could not find their jwks");
                    reject({errors: [{
                            error: 'could not find their jwks'
                        }]});
                }
                else {
                    const currentKeySet = aqfr.keyStore.current;

                    const nonce = new Uint8Array(nacl.box.nonceLength);
                    for (var i = 0; i < nonce.length; i++) nonce[i] = (32 + i)
                        & 0xff;
                    const decodeMsg = decodeUTF8(JSON.stringify(msg));
                    const box =
                        encodeBase64(
                            nacl.box(decodeMsg, nonce, decodeBase64(
                                theirJwkRecord.data.data_json.enc.key),
                                currentKeySet.enc.keyPair.secretKey));

                    const encrypted = {
                        delivery_type: 'nacl_box',
                        source_jku: currentKeySet.xrpHash,
                        source_kid: currentKeySet.enc.kid,
                        target_jku: theirJwkRecord.hash,
                        target_kid: theirJwkRecord.data.data_json.enc.kid,
                        payload: box,
                        nonce: encodeBase64(nonce)
                    };
                    resolve(encrypted);
                }
            });
        });
    }

    async generateSignedAndEncryptedMessageWithJku( msg, jku ) {
        const aqfr = this;
        return new Promise(function (resolve, reject) {
            aqfr.getTx(jku.substring(6)).then(function(sourceKeySet) {

                if (typeof sourceKeySet.result.Memos !== 'undefined') {
                    const memo = sourceKeySet.result.Memos[0];
                    const memoType = aqfr.fromHex(memo.Memo.MemoType);
                    const memoData = aqfr.fromHex(memo.Memo.MemoData);
                    const theirJwkRecord = JSON.parse(memoData);
                    if (memoType == 'publickeys') {
                        const currentKeySet = aqfr.keyStore.current;

                        const nonce = new Uint8Array(nacl.box.nonceLength);
                        for (var i = 0; i < nonce.length; i++) nonce[i] = (32 + i)
                            & 0xff;
                        const decodeMsg = decodeUTF8(JSON.stringify(msg));
                        const box =
                            encodeBase64(
                                nacl.box(decodeMsg, nonce, decodeBase64(
                                    theirJwkRecord.data_json.enc.key),
                                    currentKeySet.enc.keyPair.secretKey));

                        const encrypted = {
                            delivery_type: 'nacl_box',
                            source_jku: currentKeySet.xrpHash,
                            source_kid: currentKeySet.enc.kid,
                            target_jku: jku,
                            target_kid: theirJwkRecord.data_json.enc.kid,
                            payload: box,
                            nonce: encodeBase64(nonce)
                        };
                        resolve(encrypted);
                    }
                    else {
                        console.log('ignoring, non jwks memo found on xrp jwks pointer');
                        reject({errors: [{
                                error: 'ignoring, non jwks memo found on xrp jwks pointer'
                            }]});
                    }
                }

            });
        });
    }

    async handleSignedAndEncryptedMessage( parsedMessage ) {
        const aqfr = this;
        return new Promise(function (resolve, reject) {
            if ( typeof parsedMessage.delivery_type !== 'undefined' &&
                parsedMessage.delivery_type == 'nacl_box') {
                const sourceJku = parsedMessage.source_jku;
                const targetJku = parsedMessage.target_jku;
                var hash = sourceJku;
                if (sourceJku.startsWith('xrp://')) {
                    hash = sourceJku.substring(6);
                }
                aqfr.getTx(hash).then(function (sourceKeySet) {

                    if (typeof sourceKeySet.result.Memos !== 'undefined') {
                        const memo = sourceKeySet.result.Memos[0];
                        const memoType = aqfr.fromHex(memo.Memo.MemoType);
                        const memoData = aqfr.fromHex(memo.Memo.MemoData);
                        const parsedSourceKeySet = JSON.parse(memoData);
                        if (memoType == 'publickeys') {
                            const open = nacl.box.open(decodeBase64(parsedMessage.payload),
                                decodeBase64(parsedMessage.nonce),
                                decodeBase64(parsedSourceKeySet.data_json.enc.key),
                                aqfr.keyStore[targetJku].enc.keyPair.secretKey);
                            resolve({ msg_raw : parsedMessage,
                                source_xrp_address : sourceKeySet.result.Account,
                                target_xrp_address : aqfr.xrpAddress,
                                msg_payload : JSON.parse(encodeUTF8(open))  });
                        }
                        else {
                            console.log('ignoring, non jwks memo found on xrp jwks pointer');
                            reject({errors: [{
                                    error: 'ignoring, non jwks memo found on xrp jwks pointer'
                                }]});
                        }
                    }
                }).catch(function(error) {
                });
            }
            else {
                console.log('non nacl_box delivery type');
                reject({errors: [{
                        error: 'non nacl_box  delivery type'
                    }]});
            }
        });

    }



    async sendSignedAndEncryptedMessage( data, destinationAddress, amount="20" ) {

        const theirJwkRecord = await this.getFirstRecordByType(destinationAddress, 'publickeys');
        if (typeof theirJwkRecord.hash === 'undefined') {
            console.log("could not find their jwks");
            return;
        }
        const currentKeySet = this.keyStore.current;

        const nonce = new Uint8Array(nacl.box.nonceLength);
        for (var i = 0; i < nonce.length; i++) nonce[i] = (32+i) & 0xff;
        const msg = decodeUTF8(JSON.stringify(data));
        const box =
            encodeBase64(
                nacl.box(msg, nonce, decodeBase64(theirJwkRecord.data.data_json.enc.key), currentKeySet.enc.keyPair.secretKey));

        const encrypted = {
            delivery_type : 'nacl_box',
            source_jku : currentKeySet.xrpHash,
            source_kid: currentKeySet.enc.kid,
            target_jku : theirJwkRecord.hash,
            target_kid: theirJwkRecord.data.data_json.enc.kid,
            payload: box,
            nonce : encodeBase64( nonce )
        };
        return await this.pay(destinationAddress, amount,
            'message',
            encrypted);
    }

    async signSaveAndNotify( key, data, notificationAddress ) {
        const currentKeySet = this.keyStore.current;
        const sig = nacl.sign.detached(decodeUTF8(JSON.stringify(data)), currentKeySet.sig.keyPair.secretKey);
        return this.saveAndNotify( key, { delivery_type : 'nacl_signed',
            jku : currentKeySet.xrpHash, kid: currentKeySet.sig.kid, payload: JSON.stringify(data), sig : encodeBase64( sig )
        }, notificationAddress);
    }

    createCryptoCondition() {
        const preimageData = crypto.randomBytes(32);
        const myFulfillment = new cc.PreimageSha256();
        myFulfillment.setPreimage(preimageData);
        return {
            condition: myFulfillment.getConditionBinary().toString('hex').toUpperCase(),
            fulfillment: myFulfillment.serializeBinary().toString('hex').toUpperCase()
        }
    }

    finishEscrow(txnHash, fullfillment) {
        const aqfr = this;
        return new Promise(function(resolve, reject) {
            aqfr.getTx(txnHash).then(function(tx) {
                console.log('Info Tx:' + JSON.stringify(tx, null, 2));
                if (tx.result.TransactionType !== 'EscrowCreate') {
                    reject({error: 'Cannot finish escrow.  The txn hash provided does not point to an escrow.'});
                    return;
                }

                if (tx.result.meta.TransactionResult !== 'tesSUCCESS') {
                    reject({error: 'Cannot finish escrow.  The EscrowCreate txn did not succeed'});
                    return;
                }
                if (typeof tx.result.Condition === 'undefined') {
                    reject({error: 'No condition found on escrow'});
                    return;
                }

                if (typeof tx.result.FinishAfter !== 'undefined' && aqfr.jsDateToRippleSeconds(new Date()) < tx.result.FinishAfter) {
                    reject({error: 'Cannot finish escrow.  The FinishAfter time has not yet passed.'});
                    return;
                }
                const ownerAccount = tx.result.Account;
                const offerSequence = tx.result.Sequence;

                     const escrowRequest = {
                        "Account": aqfr.xrpAddress,
                        "TransactionType": "EscrowFinish",
                        "Owner": ownerAccount,
                        "OfferSequence": offerSequence,
                        "Condition" : tx.result.Condition,
                        "Fulfillment": fullfillment,
                        "Fee": (330+(10*2)).toString(),
                        "Sequence": aqfr.sequence+1,
                    };

                    console.log('finish req:' + JSON.stringify(escrowRequest, null, 2));

                    try {
                        const signed = aqfr.api.sign(JSON.stringify(escrowRequest),
                            aqfr.secret);
                        const txnObj = {
                            "id": aqfr.id++,
                            "command": "submit",
                            "tx_blob": signed.signedTransaction
                        };
                        aqfr.requestMap[txnObj.id.toString()] = function (data) {
                            aqfr.setSequence(data.result.tx_json.Sequence,
                                'domain');
                            resolve(data.result);
                        };
                        //console.log('--> account set');
                        aqfr.ws.send(JSON.stringify(txnObj));
                    }
                    catch(error) {
                        console.log('caught error:' + error);
                        reject({error: error});
                    }
                });

        });
    }

    getEscrow(txnHash) {

        const aqfr = this;

        return new Promise(function(resolve, reject) {
            aqfr.getTx(txnHash).then(function(tx) {
                if (tx.result.TransactionType !== 'EscrowCreate') {
                    reject({error: 'Cannot find escrow.  The txn hash provided does not point to an escrow.'});
                    return;
                }

                if (tx.result.meta.TransactionResult !== 'tesSUCCESS') {
                    reject({error: 'Cannot find escrow.  The EscrowCreate txn did not succeed'});
                    return;
                }

                const fetchSpecificEscrow = {
                    "id": aqfr.id++,
                    "command": "account_objects",
                    "account": tx.result.Account,
                    "ledger_index": "validated",
                    "type": "escrow"
                };
                aqfr.requestMap[fetchSpecificEscrow.id.toString()] = function (data) {

                    resolve(data.result.account_objects.filter((ao)=>ao.Condition === tx.result.Condition));
                };

                aqfr.ws.send(JSON.stringify(fetchSpecificEscrow));
            }).catch(function(error) {
                reject(error);
            });
        });

    }

    cancelEscrow(txnHash) {
        const aqfr = this;
        return new Promise(function(resolve, reject) {
            aqfr.getTx(txnHash).then(function(tx) {
               console.log('Info Tx:' + JSON.stringify(tx, null, 2));
               if (tx.result.TransactionType !== 'EscrowCreate') {
                   reject({error: 'Cannot cancel escrow.  The txn hash provided does not point to an escrow.'});
                   return;
               }
               if (typeof tx.result.CancelAfter === 'undefined') {
                   reject({error: 'Cannot cancel escrow.  The EscrowCreate txn does not have a CancelAfter field'});
                   return;
               }
               if (tx.result.meta.TransactionResult !== 'tesSUCCESS') {
                   reject({error: 'Cannot cancel escrow.  The EscrowCreate txn did not succeed'});
                   return;
               }
               if (aqfr.jsDateToRippleSeconds(new Date()) < tx.result.CancelAfter) {
                   reject({error: 'Cannot cancel escrow.  The CancelAfter time has not yet passed.'});
                   return;
               }
               const ownerAccount = tx.result.Account;
               const offerSequence = tx.result.Sequence;

                aqfr.fees().then(function(fees) {

                    const escrowRequest = {
                        "Account": aqfr.xrpAddress,
                        "TransactionType": "EscrowCancel",
                        "Owner": ownerAccount,
                        "OfferSequence": offerSequence,
                        "Fee": fees.toString(),
                        "Sequence": aqfr.sequence+1,
                    };

                    console.log('cancel req:' + JSON.stringify(escrowRequest, null, 2));

                    try {
                        const signed = aqfr.api.sign(JSON.stringify(escrowRequest),
                            aqfr.secret);
                        const txnObj = {
                            "id": aqfr.id++,
                            "command": "submit",
                            "tx_blob": signed.signedTransaction
                        };
                        aqfr.requestMap[txnObj.id.toString()] = function (data) {
                            aqfr.setSequence(data.result.tx_json.Sequence,
                                'domain');
                            resolve(data.result);
                        };
                        //console.log('--> account set');
                        aqfr.ws.send(JSON.stringify(txnObj));
                    }
                    catch(error) {
                        console.log('caught error:' + error);
                        reject({error: error});
                    }
                });
            }).catch(function(error) {
                reject(error);
            });
        });
    }

    createEscrowWithExpiration( amountInDrops, cryptoCondition, cancelAfter, destination, destinationTag ) {
        const aqfr = this;
        return new Promise(function (resolve, reject) {

            const cancelD = aqfr.rippleSecondsToJsDate(parseInt(cancelAfter));
            if (cancelD.getTime() <= new Date().getTime()) {
                reject({error: 'cancel after must be after now'});
                return;
            }

            aqfr.fees().then(function(fees) {

                const escrowRequest = {
                    "Account": aqfr.xrpAddress,
                    "TransactionType": "EscrowCreate",
                    "Fee": fees.toString(),
                    "Sequence": aqfr.sequence+1,
                    "Amount": amountInDrops,
                    "Destination": destination,
                    "CancelAfter":  cancelAfter ,
                    "Condition": cryptoCondition
                };
                if (typeof destinationTag !== 'undefined') {
                    escrowRequest.DestinationTag = destinationTag;
                }
                try {
                    const signed = aqfr.api.sign(JSON.stringify(escrowRequest),
                        aqfr.secret);
                    const txnObj = {
                        "id": aqfr.id++,
                        "command": "submit",
                        "tx_blob": signed.signedTransaction
                    };
                    aqfr.requestMap[txnObj.id.toString()] = function (data) {
                        aqfr.setSequence(data.result.tx_json.Sequence,
                            'domain');
                        resolve(data.result);
                    };

                    aqfr.ws.send(JSON.stringify(txnObj));
                }
                catch(error) {
                    console.log('caught error:' + error);
                    reject({error: error});
                }
            });
        });
    }

    jsDateToRippleSeconds( d ) {
        const t = d.getTime() / 1000;
        return parseInt(new Number(t-946684800).toFixed(0));
    }

    rippleSecondsToJsDate( s ) {
        return new Date((946684800*1000)+(s*1000));
    }

    getSignerList() {
        const self = this;
        return new Promise(async function(resolve, reject) {
            const getSignerList = {
                "id": self.id++,
                "command": "account_objects",
                "account": self.xrpAddress,
                "ledger_index": "validated",
                "type": "signer_list"
            };
            console.log('signer list request:' + JSON.stringify(getSignerList, null, 2));
            self.requestMap[getSignerList.id.toString()] = function (data) {
                console.log('signer list response:' +JSON.stringify(data, null, 2));
                resolve(data.result.account_objects.filter((ao)=>ao.LedgerEntryType === 'SignerList'));
            };
            self.ws.send(JSON.stringify(getSignerList));
        });
    }

    getSignerListForXrpAddress( xrpAddress ) {
        const self = this;
        return new Promise(async function(resolve, reject) {
            const getSignerList = {
                "id": self.id++,
                "command": "account_objects",
                "account": xrpAddress,
                "ledger_index": "validated",
                "type": "signer_list"
            };
            self.requestMap[getSignerList.id.toString()] = function (data) {
                resolve(data.result.account_objects.filter((ao)=>ao.LedgerEntryType === 'SignerList'));
            };
            self.ws.send(JSON.stringify(getSignerList));
        });
    }

    setSignerList( signerList ) {
        const self = this;

        return new Promise(async function(resolve, reject) {
            if (typeof signerList === 'undefined') {
                reject({error: 'The request must include a signer list.'});
                return;
            }

            // if the current user is to be included, the maximum number of signers is 7 (8-1)
            if (signerList.length > 8) {
                reject({error: 'There can be no more than 7 signers.'});
                return;
            }
            const signerEntries = [];
            signerList.forEach( (signer) => {
              signerEntries.push( {
                  SignerEntry: {
                      Account: signer,
                      SignerWeight: 1
                  }
              });
            });


            self.fees().then(function(fees) {
                const txn = {
                    Flags: 0,
                    TransactionType: "SignerListSet",
                    Sequence: self.sequence+1,
                    Account: self.xrpAddress,
                    Fee: fees.toString(),
                    SignerQuorum: signerList.length
                };

                if (signerList.length > 0) {
                   txn.SignerEntries = signerEntries;
                }

                console.log('SignerListRequest:' + JSON.stringify(txn, null, 2));
                try {
                    const signed = self.api.sign(JSON.stringify(txn),
                        self.secret);
                    const txnObj = {
                        "id": self.id++,
                        "command": "submit",
                        "tx_blob": signed.signedTransaction
                    };
                    self.requestMap[txnObj.id.toString()] = function (data) {
                        self.setSequence(data.result.tx_json.Sequence,
                            'domain');
                        console.log('SigningListResponse:' + JSON.stringify(data, null, 2));

                        self.waitUntilFinal(data.result).then(
                            function (txnResponse) {
                                if (txnResponse.engine_result
                                    !== 'tesSUCCESS') {
                                    reject({
                                        error: 'The submitted set signer list transaction was not successful:'
                                            + txnResponse.engine_result
                                    });
                                } else {
                                    self.getSignerList().then(
                                        function (returnedList) {
                                            resolve(returnedList);
                                            self.emit('aqfr-signer-list', {
                                                hash: data.result.tx_json.hash,
                                                signerList: returnedList
                                            });
                                        }).catch(function (error) {
                                        reject(error);
                                    });
                                }
                            });
                    };
                    self.ws.send(JSON.stringify(txnObj));
                }
                catch(error) {
                    console.log('caught error:' + JSON.stringify(error, null, 2));
                    reject(error);
                }
            }).catch(function(error) {
                reject(error);
            });

        });

    }

    signMultiSigTransaction( preparedTransaction ) {
         return this.api.sign(JSON.stringify(preparedTransaction), this.secret, {'signAs': this.xrpAddress}).signedTransaction;    }

    combineMultiSigTransaction( signedTransactions  ) {
        return this.api.combine(signedTransactions);
    }

    prepareMultiSigTransaction( numSigners, transaction ) {
        const self = this;
        return new Promise(async function(resolve, reject) {
            const accountInfo = await self.promiseWsAccountInfo(transaction.Account);
            const fees = await self.fees();
            transaction.Fee = (fees*(numSigners+1)).toString();
            transaction.Sequence =  (typeof accountInfo.result !== 'undefined' ? accountInfo.result.account_data.Sequence: 0);
            resolve(transaction);

        });
    }

    submitSignedTransaction( signed ) {
        const aqfr = this;

        return new Promise(async function(resolve, reject) {
            const txnObj = {
                "id": aqfr.id++,
                "command": "submit",
                "tx_blob": signed
            };
            aqfr.requestMap[txnObj.id.toString()] = function (data) {
                aqfr.waitUntilFinal(data.result).then(function(finalized) {
                    resolve(data);
                }).catch(function(error) {
                    reject(error);
                });
            };
            aqfr.ws.send(JSON.stringify(txnObj));
        });
    }

    createEscrowWithExpirationAndFinishAfter( amountInDrops, cryptoCondition, cancelAfter, finishAfter, destination, destinationTag ) {

        const aqfr = this;
        return new Promise(function (resolve, reject) {
            if (cancelAfter >= finishAfter) {
                reject({error: 'cancelafter must be before finishafter'});
                return;
            }
            const cancelD = aqfr.rippleSecondsToJsDate(cancelAfter);
            if (cancelD.getTime() <= new Date().getTime()) {
                reject({error: 'cancel after must be after now'});
                return;
            }
            const finishD = aqfr.rippleSecondsToJsDate(finishAfter);
            if (finishD.getTime() <= new Date().getTime()) {
                reject({error: 'finish after must be after now'});
                return;
            }
            aqfr.fees().then(function(fees) {
                const escrowRequest = {
                    "Account": aqfr.xrpAddress,
                    "TransactionType": "EscrowCreate",
                    "Fee": fees.toString(),
                    "Sequence": aqfr.sequence+1,
                    "Amount": amountInDrops,
                    "Destination": destination,
                    "CancelAfter": cancelAfter,
                    "FinishAfter": finishAfter,
                    "Condition": cryptoCondition
                };
                if (typeof destinationTag !== 'undefined') {
                    escrowRequest.DestinationTag = destinationTag;
                }

                const signed = aqfr.api.sign(JSON.stringify(escrowRequest), aqfr.secret);
                const txnObj = {
                    "id": aqfr.id++,
                    "command": "submit",
                    "tx_blob": signed.signedTransaction
                };
                aqfr.requestMap[txnObj.id.toString()] = function (data) {
                    aqfr.setSequence(data.result.tx_json.Sequence, 'domain');
                    resolve(data.result);
                };
                //console.log('--> account set');
                aqfr.ws.send(JSON.stringify(txnObj));
            });
        });
    }

    sign( data ) {
        const currentKeySet = this.keyStore.current;
        const sig = nacl.sign.detached(decodeUTF8(JSON.stringify(data)), currentKeySet.sig.keyPair.secretKey);
        return { delivery_type : 'nacl_signed',
            jku : currentKeySet.xrpHash, kid: currentKeySet.sig.kid, payload: JSON.stringify(data), sig : encodeBase64( sig )
        };
    }

    verify( parsedMessage ) {
        const aqfr = this;
        return new Promise(function (resolve, reject) {
            if ( typeof parsedMessage.delivery_type !== 'undefined' &&
                parsedMessage.delivery_type == 'nacl_signed') {

                var hash = parsedMessage.jku;
                console.log('verification hash pre:' + hash);
                if (hash.startsWith('xrp://')) {
                    hash = hash.substring(6);
                }
                console.log('verification hash post:' + hash);

                aqfr.getTx(hash).then(function (sourceKeySet) {
                    if (typeof sourceKeySet.result.Memos !== 'undefined') {
                        const memo = sourceKeySet.result.Memos[0];
                        const memoType = aqfr.fromHex(memo.Memo.MemoType);
                        const memoData = aqfr.fromHex(memo.Memo.MemoData);
                        const parsedSourceKeySet = JSON.parse(memoData);
                        if (memoType == 'publickeys') {
                            const open = nacl.sign.detached.verify(decodeUTF8(parsedMessage.payload), decodeBase64( parsedMessage.sig), decodeBase64(parsedSourceKeySet.data_json.sig.key));

                            resolve({ msg_raw : parsedMessage,
                                signing_address : sourceKeySet.result.Account,
                                signing_key : parsedSourceKeySet.data_json.sig.key,
                                singing_hash : hash,
                                signing_hash : hash,
                                status : 'verified' });
                        }
                        else {
                            console.log('ignoring, non jwks memo found on xrp jwks pointer');
                            resolve({ msg_raw : parsedMessage,
                                status : 'not_verified' });
                        }
                    }
                });
            }
            else {
                resolve({ msg_raw : parsedMessage,
                    status : 'not_verified' });
            }
        });

    }


    async rotateKeys( notificationAddress ) {
        const signingKp = nacl.sign.keyPair();
        const base64EncodedSigningKey = encodeBase64(signingKp.publicKey);
        const signingKid = Math.random().toString(36).substring(2, 6);

        const encKp = nacl.box.keyPair();
        const base64EncodedEncKey = encodeBase64(encKp.publicKey);
        const encKid = Math.random().toString(36).substring(2, 6);
        const pk = {
            sig : {
                alg: "ed25519",
                kid: signingKid,
                use: 'sig',
                key: base64EncodedSigningKey
            },
            enc : {
                alg: "x25519-xsalsa20-poly1305",
                kid: encKid,
                use: 'enc',
                key: base64EncodedEncKey
            }
        };

        const jwkXrpHash = await this.saveAndNotify('publickeys', pk,
            notificationAddress );

        await this.saveJwks( jwkXrpHash, {
            sig : {
                alg: "ed25519",
                kid: signingKid,
                use: 'sig',
                keyPair: signingKp
            },
            enc : {
                alg: "x25519-xsalsa20-poly1305",
                kid: encKid,
                use: 'enc',
                keyPair: encKp
            },
            xrpHash : jwkXrpHash
        } );
        this.emit('aqfr-public-keys', {
            hash : jwkXrpHash, publicKeys : pk
        });

        return jwkXrpHash;
    }

    async saveJwks(jwkXrpHash, store) {
        this.keyStore.current = store;
        this.keyStore[jwkXrpHash] = store;
        if (this.keyStoreListener !== null && typeof this.keyStoreListener.onAqfrKeySet !== 'undefined') {
            this.keyStoreListener.onAqfrKeySet(jwkXrpHash, store);
        }
    }

    handleMessage( fromAddress, memoData, transaction, callback ) {
        const aqfr = this;
        const parsedMessage = JSON.parse(memoData);
        if ( typeof parsedMessage.data_json.delivery_type !== 'undefined' &&
            parsedMessage.data_json.delivery_type == 'nacl_box') {
            const sourceJku = parsedMessage.data_json.source_jku;
            const targetJku = parsedMessage.data_json.target_jku;
            var hash = sourceJku;
            if (sourceJku.startsWith('xrp://')) {
                hash = sourceJku.substring(6);
            }
            aqfr.getTx(hash).then(function (sourceKeySet) {
                if (typeof sourceKeySet.result.Memos !== 'undefined') {
                    const memo = sourceKeySet.result.Memos[0];
                    const memoType = aqfr.fromHex(memo.Memo.MemoType);
                    const memoData = aqfr.fromHex(memo.Memo.MemoData);
                    const parsedSourceKeySet = JSON.parse(memoData);
                    if (memoType == 'publickeys') {
                        const open = nacl.box.open(decodeBase64(parsedMessage.data_json.payload),
                            decodeBase64(parsedMessage.data_json.nonce),
                            decodeBase64(parsedSourceKeySet.data_json.enc.key),
                            aqfr.keyStore[targetJku].enc.keyPair.secretKey);
                        aqfr.emit('received-message', { msg_raw : parsedMessage,
                            msg_payload : JSON.parse(encodeUTF8(open)), transaction: transaction });
                        callback({ msg_raw : parsedMessage,
                            msg_payload : JSON.parse(encodeUTF8(open)), transaction: transaction });
                        aqfr.promiseWsAccountInfo(aqfr.xrpAddress).then(function(accountInfo) {
                            // noop
                        });
                    }
                    else {
                        console.log('ignoring, non jwks memo found on xrp jwks pointer');
                    }
                }
            });
        }
    }

    handleNotification( memoData , callback) {
        const aqfr = this;
        const txnHash = JSON.parse(memoData).data_json.hash;
        var hash = txnHash;
        if (txnHash.startsWith('xrp://')) {
            hash = txnHash.substring(6);
        }
        this.getTx(hash).then(function (data) {
            if (typeof data.result.Memos !== 'undefined') {
                const memo = data.result.Memos[0];
                const memoType = aqfr.fromHex(memo.Memo.MemoType);
                const memoData = aqfr.fromHex(memo.Memo.MemoData);
                const parsedObj = JSON.parse(memoData);
                // this is a jws
                if (typeof parsedObj.data_json.jku !== 'undefined' &&
                    typeof parsedObj.data_json.kid !== 'undefined' &&
                    typeof parsedObj.data_json.payload !== 'undefined' &&
                    typeof parsedObj.data_json.sig !== 'undefined') {

                    const jku = parsedObj.data_json.jku;
                    const jkuHash = jku.substring(6);

                    aqfr.getTx(jkuHash).then(function (jwks) {
                        if (typeof jwks.result.Memos !== 'undefined') {
                            const jwksMemo = jwks.result.Memos[0];
                            const jwksMemoType = aqfr.fromHex(
                                jwksMemo.Memo.MemoType);
                            const jwksMemoData = aqfr.fromHex(
                                jwksMemo.Memo.MemoData);
                            if (jwksMemoType == 'publickeys') {
                                const parsedKeyRecord =JSON.parse(jwksMemoData);
                                if (nacl.sign.detached.verify(decodeUTF8(parsedObj.data_json.payload), decodeBase64( parsedObj.data_json.sig), decodeBase64(parsedKeyRecord.data_json.sig.key))) {
                                    aqfr.emit('received-verified-notification', { notification : parsedObj, msg_type: memoType, jws: true, verification: parsedKeyRecord.data_json.sig,
                                        msg_payload : JSON.parse( parsedObj.data_json.payload ) });

                                    callback({ notification : parsedObj, msg_type: memoType, jws: true, verification: parsedKeyRecord.data_json.sig,
                                        msg_payload : JSON.parse( parsedObj.data_json.payload ), transaction: data });
                                }
                                else {
                                    console.log("not verified:" + parsedObj.data_json.payload);
                                }
                            }
                            else {
                                console.log('ignoring, non jwks memo found on xrp jwks pointer');
                            }
                        }
                    });
                }
                // this is a non jws
                else {
                    aqfr.emit('received-non-verified-notification', { notification : parsedObj, msg_type: memoType, jws: false, msg_payload : parsedObj.data_json, transaction: data });
                    callback({ notification : parsedObj, msg_type: memoType, jws: false, msg_payload : parsedObj.data_json, transaction: data });
                }
            }
        });
    }

    reSubscribe() {
        const aqfr = this;
        this.subscribeCallback = function (subscriptionResponse) {
            const parsedResponse = JSON.parse(subscriptionResponse);

            if (typeof parsedResponse.transaction !== 'undefined'
                && typeof parsedResponse.transaction.Memos !== 'undefined') {
                const memo = parsedResponse.transaction.Memos[0];
                const memoType = aqfr.fromHex(memo.Memo.MemoType);
                const memoData = aqfr.fromHex(memo.Memo.MemoData);
                if (memoType == 'notification') {
                    aqfr.handleNotification(memoData, aqfr.dataCallback);
                }
                else if (memoType == 'message') {
                    if (this.xrpAddress != null && parsedResponse.transaction.Destination == this.xrpAddress) {
                        aqfr.handleMessage(parsedResponse.transaction.Account,  memoData, parsedResponse.transaction,  aqfr.dataCallback);
                    }
                }
                else if (memoType == 'publickeys') {
                    if (this.xrpAddress != null && typeof JSON.parse(memoData).address !== 'undefined' &&
                        JSON.parse(memoData).address == this.xrpAddress) {
                        console.log('our public keys received in subscription response');
                    }
                    else {
                        // this is a non-notification message.
                        console.log(
                            'Subscription Other Public Keys Response: ' + aqfr.xrpAddress
                            + ' -> ' + memoData);
                    }
                }
                else {
                    // this is a non-notification message.
                    console.log(
                        'Subscription Other Response: ' + aqfr.xrpAddress
                        + ' -> ' + (memoType) + ':' + memoData);
                }
            } else {
                // this is the subscription ack message.
                if (typeof JSON.parse(subscriptionResponse).id !== 'undefined'
                    && JSON.parse(subscriptionResponse).id
                    == 'Subscribe to Central') {
                    console.log(
                        "Subscription Inited: " + aqfr.xrpAddress);
                }
            }

            // let us emit the transaction...
            if (typeof parsedResponse.transaction !== 'undefined' && parsedResponse.transaction.TransactionType !== 'undefined' &&
                parsedResponse.transaction.TransactionType === 'Payment') {
                if (typeof parsedResponse.transaction.Account !== 'undefined') {
                    if (typeof parsedResponse.transaction.hash !== 'undefined') {
                        if (parsedResponse.transaction.Account === aqfr.xrpAddress) {
                            const txnEvent = {
                                transaction_type : 'Payment',
                                action: 'sent',
                                hash : parsedResponse.transaction.hash ,
                                txn : parsedResponse.transaction,
                                source_xrp_address: aqfr.xrpAddress,
                                destination_xrp_address: parsedResponse.transaction.Destination,
                                amount: parsedResponse.transaction.Amount
                            };
                            if (typeof parsedResponse.transaction.DestinationTag !== 'undefined') {
                                txnEvent.destination_xrp_address_tag = parsedResponse.transaction.DestinationTag;
                            }
                            if (parsedResponse.engine_result === 'tesSUCCESS') {
                                aqfr.emit('aqfr-payment', txnEvent);
                            }
                            else {
                                console.log('Not emmiting non-successful payment.  engine_result=' + parsedResponse.engine_result + ', Event=' + JSON.stringify(txnEvent));
                            }

                        }
                        else if (typeof parsedResponse.transaction.Destination !== 'undefined' &&
                            parsedResponse.transaction.Destination === aqfr.xrpAddress) {
                            const txnEvent = {
                                transaction_type : 'Payment',
                                action: 'received',
                                hash : parsedResponse.transaction.hash,
                                txn: parsedResponse.transaction,
                                source_xrp_address: parsedResponse.transaction.Account,
                                destination_xrp_address: aqfr.xrpAddress,
                                amount: parsedResponse.transaction.Amount
                            };
                            if (typeof parsedResponse.transaction.DestinationTag !== 'undefined') {
                                txnEvent.destination_xrp_address_tag = parsedResponse.transaction.DestinationTag;
                            }
                            if (parsedResponse.engine_result === 'tesSUCCESS') {
                                aqfr.emit('aqfr-payment', txnEvent);
                            }
                            else {
                                console.log('Not emmiting non-successful payment.  engine_result=' + parsedResponse.engine_result + ', Event=' + JSON.stringify(txnEvent));
                            }
                        }
                    }
                }
            }
        };
        const accounts = [];
        if (this.xrpAddress != null) {
            accounts.push(this.xrpAddress);
        }
        if (typeof this.network !== 'undefined') {
            accounts.push(this.network);
        }
        const request = {
            "id": "Subscribe to Central",
            "command": "subscribe",
            "accounts": accounts
        };
        // console.log('--> subscribe to central:' + JSON.stringify(request));
        this.ws.send(JSON.stringify(request));
    }

    subscribe(callback, subscribeAddress) {

        if (typeof subscribeAddress !== 'undefined') {
            this.network = subscribeAddress;
        }
        this.network = subscribeAddress;
        this.dataCallback = callback;

        this.reSubscribe();
    }

    async setDomain( domain ) {
        const aqfr = this;

        return new Promise(function (resolve, reject) {

            aqfr.fees().then(function(fees) {
                const txn = {
                    "TransactionType": "AccountSet",
                    "Account": aqfr.xrpAddress,
                    "Sequence": aqfr.sequence+1,
                    "Domain": aqfr.toHex(domain).toUpperCase(),
                    "Fee": fees.toString()
                };
                const signed = aqfr.api.sign(JSON.stringify(txn), aqfr.secret);
                const txnObj = {
                    "id": aqfr.id++,
                    "command": "submit",
                    "tx_blob": signed.signedTransaction
                };
                aqfr.requestMap[txnObj.id.toString()] = function (data) {
                    aqfr.setSequence(data.result.tx_json.Sequence, 'domain');
                    resolve(data.result);
                };
                //console.log('--> account set');
                aqfr.ws.send(JSON.stringify(txnObj));
            });


        });

    }

    async ping() {
        const aqfr = this;
        return new Promise(function (resolve, reject) {
            const request = {
                "id": aqfr.id++,
                "command": "ping"
            };
            aqfr.requestMap[request.id.toString()] = function (data) {
                resolve(data);
            };
            //console.log('--> ping');
            aqfr.ws.send(JSON.stringify(request));
        });
    }

    async fees() {
        const aqfr = this;

        return new Promise(function (resolve, reject) {
            const request = {
                "id": aqfr.id++,
                "command": "server_info"
            };
            aqfr.requestMap[request.id.toString()] = function (data) {
                let fee = (data.result.info.validated_ledger.base_fee_xrp * data.result.info.load_factor)*1000000;
                console.log('fee:' + fee + ', baseFeeXrp:' + data.result.info.validated_ledger.base_fee_xrp + ', loadFactor: ' +
                    data.result.info.load_factor + ', timesEachOther:' +
                    (data.result.info.validated_ledger.base_fee_xrp * data.result.info.load_factor) );

                if (fee > 1000) {
                    console.log('fee greater than 1000, setting to 1000');
                    fee = 1000;
                }
                resolve(fee);
            };
            //console.log('--> ping');
            aqfr.ws.send(JSON.stringify(request));
        });

    }

    async feesNoMatter() {
        const aqfr = this;

        return new Promise(function (resolve, reject) {
            const request = {
                "id": aqfr.id++,
                "command": "server_info"
            };
            aqfr.requestMap[request.id.toString()] = function (data) {
                let fee = (data.result.info.validated_ledger.base_fee_xrp * data.result.info.load_factor)*1000000;
                console.log('no fee:' + fee + ', baseFeeXrp:' + data.result.info.validated_ledger.base_fee_xrp + ', loadFactor: ' +
                    data.result.info.load_factor + ', timesEachOther:' +
                    (data.result.info.validated_ledger.base_fee_xrp * data.result.info.load_factor) );


                resolve(fee.toFixed());
            };
            //console.log('--> ping');
            aqfr.ws.send(JSON.stringify(request));
        });

    }

    async promiseWsAccountInfo(xrpAddress) {
        const aqfr = this;
        return new Promise(function (resolve, reject) {
            const request = {
                "id": aqfr.id++,
                "command": "account_info",
                "account": xrpAddress
            };
            aqfr.requestMap[request.id.toString()] = function (data) {
                if (xrpAddress == aqfr.xrpAddress) {
                    if (typeof data.error !== 'undefined') {
                        console.log('error received on account balance:' + data.error);
                    }
                    else if (aqfr.balance.balance != data.result.account_data.Balance) {
                        aqfr.balance.balance = data.result.account_data.Balance;
                        aqfr.emit('account-balance', data.result.account_data.Balance);
                    }

                }
                resolve(data);
            };
            //console.log('--> account info');
            aqfr.ws.send(JSON.stringify(request));
        });

    }

    async getFirstRecordByType(xrpAddress, recordType) {
        const aqfr = this;
        const data = await this.promiseWsAccountInfo(xrpAddress);
        var prevHash = 'NONE';
        if (typeof data.result !== 'undefined' && typeof data.result.account_data.Domain !== 'undefined') {
            const domain = data.result.account_data.Domain;
            const decodedDomain = aqfr.fromHex(domain);
            if (decodedDomain.startsWith('xrp://')) {
                prevHash = decodedDomain.substring(6);
            }
        }

        return new Promise(function (resolve, reject) {
            if (prevHash == 'NONE') {
                console.log('no records found');
                resolve( {} );
            } else {
                resolve( aqfr.nagivateRecordsByType(prevHash, recordType));
            }
        });

    }

    nagivateRecordsByType(prevHash, recordType ) {
        const aqfr = this;
        const thisHash = prevHash;
        return new Promise(function(resolve, reject) {
            aqfr.getTx(prevHash).then(function (data) {

                if (typeof data.result !== 'undefined' && typeof data.result.Memos
                    !== 'undefined') {
                    const memo = data.result.Memos[0];
                    const memoType = aqfr.fromHex(memo.Memo.MemoType);
                    const memoData = aqfr.fromHex(memo.Memo.MemoData);
                    const parsedMemoData = JSON.parse(memoData);
                    if (memoType == recordType) {
                        resolve( { hash: 'xrp://' + thisHash, data: parsedMemoData});
                    }
                    else if (parsedMemoData.prev_hash != 'xrp://NONE') {
                        var prevHash = parsedMemoData.prev_hash;
                        if (parsedMemoData.prev_hash.startsWith('xrp://')) {
                            prevHash = parsedMemoData.prev_hash.substring(6);
                        }
                        resolve(aqfr.nagivateRecordsByType(prevHash, recordType));
                    } else {
                        resolve({});
                    }
                } else {
                    resolve({});
                }
            });

        });

    }


    async getTx(hash) {
        const aqfr = this;
        return new Promise(function (resolve, reject) {
            const request = {
                "id": aqfr.id++,
                "command": "tx",
                "transaction": hash,
                "binary": false
            };
            aqfr.requestMap[request.id.toString()] = function (data) {
                resolve(data);
            };
            aqfr.ws.send(JSON.stringify(request));
        });

    }

    async straightPayWithTag(destinationAddress, destinationTag, amount ) {
        const aqfr = this;
        return new Promise(function (resolve, reject) {

            aqfr.fees().then(function(fees) {
                const txn = {
                    "TransactionType": "Payment",
                    "Account": aqfr.xrpAddress,
                    "Destination": destinationAddress,
                    "DestinationTag" : destinationTag,
                    "Amount": amount,
                    "Fee": fees.toString(),
                    "Sequence": (aqfr.sequence+1)
                };

                const signed = aqfr.api.sign(JSON.stringify(txn), aqfr.secret);
                const txnObj = {
                    "id": aqfr.id++,
                    "command": "submit",
                    "tx_blob": signed.signedTransaction
                };

                aqfr.requestMap[txnObj.id.toString()] = function (data) {
                    aqfr.setSequence(data.result.tx_json.Sequence, 'pay');
                    resolve(data.result);
                };
                //console.log('--> pay');
                aqfr.ws.send(JSON.stringify(txnObj));
            });


        })
    }

    async signStraightPayWithTag(destinationAddress, destinationTag, amount ) {
        const aqfr = this;
        return new Promise(function (resolve, reject) {

            aqfr.fees().then(function(fees) {
                const txn = {
                    "TransactionType": "Payment",
                    "Account": aqfr.xrpAddress,
                    "Destination": destinationAddress,
                    "DestinationTag" : destinationTag,
                    "Amount": amount,
                    "Fee": fees.toString(),
                    "Sequence": (aqfr.sequence+1)
                };

                const signed = aqfr.api.sign(JSON.stringify(txn), aqfr.secret);
                resolve(signed);
            });
        });
    }

    waitUntilFinalHash( hash ) {
        const aqfr = this;

        return new Promise(function(resolve, reject) {
            console.log('Initializing wait until final hash.  hash=' + hash);
            let count = 0;
            const interval = setInterval(function() {
                console.log('Fetching txn for hash while waiting.  hash=' + hash);
                aqfr.getTx(hash).then(function(tx) {

                    if (typeof tx.result !== 'undefined' && typeof tx.result.inLedger !== 'undefined') {
                        console.log('got result while waiting.  hash=' + hash + ', engine_result:' + tx.result.meta.TransactionResult);
                        clearInterval(interval);
                        resolve({hash: tx.result.hash, engine_result: tx.result.meta.TransactionResult});
                    }
                    else {
                        console.log('result is undefined while waiting or not in ledger.  hash=' + hash + ', count=' + count);
                        count = count+1;
                        if (count > 30) {
                            clearInterval(interval);
                            reject({error: 'finalized timed out'});
                        }
                    }
                });
            }, 1000);
        });

    }


    waitUntilFinal( input ) {
        return this.waitUntilFinalHash(input.tx_json.hash);
    }

    waitUntilXrpAddressIsInitialized( xrpAddress ) {
        const aqfr = this;

        return new Promise(function(resolve, reject) {
            let count = 0;
            const interval = setInterval(function() {
                aqfr.promiseWsAccountInfo(xrpAddress).then(function(account ){
                    if (typeof account.error === 'undefined') {
                        clearInterval(interval);
                        console.log('[AQFR : WAIT UNTIL ACCOUNT] Account Initialized.  XrpAddress=' + xrpAddress );
                        resolve( account );
                    }
                    else {
                        console.log('[AQFR : WAIT UNTIL ACCOUNT] Account Not Yet Initialized.  XrpAddress=' + xrpAddress + ', Error=' + account.error + ', Count=' + (count+1) );
                        count = count+1;
                        if (count > 20) {
                            clearInterval(interval);
                            reject({error: 'Account initialization check timed out.'});
                            console.log('[AQFR : WAIT UNTIL ACCOUNT] Account Initialization Check Timed Out.  XrpAddress=' + xrpAddress + ', Count=' + (count+1) );

                        }
                    }
                })
                .catch(function(error) {
                    clearInterval(interval);
                    reject({error: 'Account initialization check timed out.', errorData: error });
                    console.log('[AQFR : WAIT UNTIL ACCOUNT] Account Initialization Received Error.  XrpAddress=' + xrpAddress + ', Error=' + (JSON.stringify(error, null, 2)) );

                });
            }, 1000);
        });

    }

    async straightPay(destinationAddress, amount ) {
        const aqfr = this;
        return new Promise(function (resolve, reject) {
            aqfr.fees().then(function(fees) {
                const fee = fees;

                const txn = {
                    "TransactionType": "Payment",
                    "Account": aqfr.xrpAddress,
                    "Destination": destinationAddress,
                    "Amount": amount,
                    "Fee": fee.toString(),
                    "Sequence": (aqfr.sequence+1)
                };

                const signed = aqfr.api.sign(JSON.stringify(txn), aqfr.secret);

                const txnObj = {
                    "id": aqfr.id++,
                    "command": "submit",
                    "tx_blob": signed.signedTransaction
                };

                aqfr.requestMap[txnObj.id.toString()] = function (data) {
                    aqfr.setSequence(data.result.tx_json.Sequence, 'pay');
                    resolve(data.result);
                };
                //console.log('--> pay');
                aqfr.ws.send(JSON.stringify(txnObj));
            });
        })
    }

    async submitTransaction( signedTransaction ) {
        return new Promise(function (resolve, reject) {
             const txnObj = {
                    "id": aqfr.id++,
                    "command": "submit",
                    "tx_blob": signedTransaction
                };

                aqfr.requestMap[txnObj.id.toString()] = function (data) {
                    aqfr.setSequence(data.result.tx_json.Sequence, 'pay');
                    resolve(data.result);
                };
                //console.log('--> pay');
                aqfr.ws.send(JSON.stringify(txnObj));
            });

    }

    async signStraightPay(destinationAddress, amount ) {
        const aqfr = this;
        return new Promise(function (resolve, reject) {
            aqfr.fees().then(function(fees) {
                const fee = fees;

                const txn = {
                    "TransactionType": "Payment",
                    "Account": aqfr.xrpAddress,
                    "Destination": destinationAddress,
                    "Amount": amount,
                    "Fee": fee.toString(),
                    "Sequence": (aqfr.sequence+1)
                };

                const signed = aqfr.api.sign(JSON.stringify(txn), aqfr.secret);
                resolve(signed);
            });
        })
    }

    async payWithTag(destinationAddress, destinationTag, amount, memoKey, memoData ) {
        const aqfr = this;
        return new Promise(function (resolve, reject) {

            aqfr.fees().then(function(fees) {

                const memoRecord = {
                    "timestamp": new Date().toISOString(),
                    "data_json": memoData,
                    "address": aqfr.xrpAddress
                };

                const memoKeyHex = aqfr.toHex(memoKey).toUpperCase();
                const memoDataHex = aqfr.toHex(JSON.stringify(
                    memoRecord)).toUpperCase();

                const txn = {
                    "TransactionType": "Payment",
                    "Account": aqfr.xrpAddress,
                    "Destination": destinationAddress,
                    "DestinationTag": destinationTag,
                    "Amount": amount,
                    "Fee": fees.toString(),
                    "Memos": [
                        {
                            "Memo": {
                                "MemoType": memoKeyHex,
                                "MemoData": memoDataHex
                            }
                        }
                    ],
                    "Sequence": (aqfr.sequence + 1)
                };
                const signed = aqfr.api.sign(JSON.stringify(txn), aqfr.secret);
                const txnObj = {
                    "id": aqfr.id++,
                    "command": "submit",
                    "tx_blob": signed.signedTransaction
                };

                aqfr.requestMap[txnObj.id.toString()] = function (data) {
                    aqfr.setSequence(data.result.tx_json.Sequence, 'pay');
                    resolve(data.result);
                };
                //console.log('--> pay');
                aqfr.ws.send(JSON.stringify(txnObj));
            });
        })
    }

    async straightPayWithTag(destinationAddress, destinationTag, amount ) {
        const aqfr = this;
        return new Promise(function (resolve, reject) {

            aqfr.fees().then(function(fees) {

                const txn = {
                    "TransactionType": "Payment",
                    "Account": aqfr.xrpAddress,
                    "Destination": destinationAddress,
                    "DestinationTag": destinationTag,
                    "Amount": amount,
                    "Fee": fees.toString(),
                    "Sequence": (aqfr.sequence + 1)
                };
                const signed = aqfr.api.sign(JSON.stringify(txn), aqfr.secret);
                const txnObj = {
                    "id": aqfr.id++,
                    "command": "submit",
                    "tx_blob": signed.signedTransaction
                };

                aqfr.requestMap[txnObj.id.toString()] = function (data) {
                    aqfr.setSequence(data.result.tx_json.Sequence, 'pay');
                    resolve(data.result);
                };
                //console.log('--> pay');
                aqfr.ws.send(JSON.stringify(txnObj));
            });
        })
    }

    async pay(destinationAddress, amount, memoKey, memoData ) {
        const aqfr = this;
        return new Promise(function (resolve, reject) {
            const memoRecord = {
                "timestamp": new Date().toISOString(),
                "data_json": memoData,
                "address": aqfr.xrpAddress
            };
            aqfr.fees().then(function(fees) {
                const memoKeyHex = aqfr.toHex(memoKey).toUpperCase();
                const memoDataHex = aqfr.toHex(JSON.stringify(
                    memoRecord)).toUpperCase();

                const txn = {
                    "TransactionType": "Payment",
                    "Account": aqfr.xrpAddress,
                    "Destination": destinationAddress,
                    "Amount": amount,
                    "Fee": fees.toString(),
                    "Memos": [
                        {
                            "Memo": {
                                "MemoType": memoKeyHex,
                                "MemoData": memoDataHex
                            }
                        }
                    ],
                    "Sequence": (aqfr.sequence + 1)
                };
                const signed = aqfr.api.sign(JSON.stringify(txn), aqfr.secret);
                const txnObj = {
                    "id": aqfr.id++,
                    "command": "submit",
                    "tx_blob": signed.signedTransaction
                };

                aqfr.requestMap[txnObj.id.toString()] = function (data) {
                    aqfr.setSequence(data.result.tx_json.Sequence, 'pay');
                    resolve(data.result);
                };
                //console.log('--> pay');
                aqfr.ws.send(JSON.stringify(txnObj));
            });
        })
    }

    async setMemo(memoKey, memoData ) {
        const aqfr = this;
        const data = await this.promiseWsAccountInfo(this.xrpAddress);

        return new Promise(function (resolve, reject) {


            var prevHash = 'NONE';
            if (typeof data.result.account_data.Domain !== 'undefined') {

                const domain = data.result.account_data.Domain;
                const decodedDomain = aqfr.fromHex(domain);

                if (decodedDomain.startsWith('xrp://')) {
                    prevHash = decodedDomain.substring(6);
                }
            }

            const memoRecord = {
                "prev_hash": 'xrp://' + prevHash,
                "timestamp": new Date().toISOString(),
                "data_json": memoData,
                "address": aqfr.xrpAddress
            };

            const memoKeyHex = aqfr.toHex(memoKey).toUpperCase();
            const memoDataHex = aqfr.toHex(JSON.stringify(
                memoRecord)).toUpperCase();

            aqfr.fees().then(function(fees) {
                const txn = {
                    "TransactionType": "AccountSet",
                    "Account": aqfr.xrpAddress,
                    "Sequence": aqfr.sequence + 1,
                    "Memos": [
                        {
                            "Memo": {
                                "MemoType": memoKeyHex,
                                "MemoData": memoDataHex
                            }
                        }
                    ],
                    "Fee": fees.toString()
                };
                const signed = aqfr.api.sign(JSON.stringify(txn), aqfr.secret);
                const txnObj = {
                    "id": aqfr.id++,
                    "command": "submit",
                    "tx_blob": signed.signedTransaction
                };

                aqfr.requestMap[txnObj.id.toString()] = function (data) {
                    aqfr.setSequence(data.result.tx_json.Sequence, 'memos');
                    resolve(data.result);
                };
                //console.log('--> set memo:' + aqfr.xrpAddress + ' ' + memoKey);
                aqfr.ws.send(JSON.stringify(txnObj));
            });
        })
    }

    async connectWs(options) {
        this.wsOptions = options;
        const aqfr = this;
        return new Promise(function (resolve, reject) {
            const wsOptions = options;

            if (aqfr.ws == null || aqfr.connectedApi == false) {
                aqfr.ws = new WebSocket(typeof options.server === 'undefined'
                    ? 'wss://s1.ripple.com/'
                    : options.server);
                aqfr.api = new RippleAPI({feeCushion: 1.2});
                aqfr.ws.onopen = function open() {
                    console.log('connected');
                    aqfr.connectedApi = true;
                    aqfr.heartbeat();
                    if (aqfr.dataCallback != null) {
                        aqfr.reSubscribe();
                    }
                    aqfr.emit('aqfr-ledger-connection', {state: 'connected', server : typeof wsOptions.server === 'undefined'
                            ? 'wss://s1.ripple.com/'
                            : wsOptions.server});
                    resolve('connected');
                };

                aqfr.ws.onerror = function(error) {
                    console.log('ws error:' + JSON.stringify(error));
                };

                aqfr.ws.onclose = function close() {
                    console.log('disconnected');
                    aqfr.emit('aqfr-ledger-connection', {state: 'disconnected', server : typeof wsOptions.server === 'undefined'
                            ? 'wss://s1.ripple.com/'
                            : wsOptions.server});
                    aqfr.ws = null;
                    aqfr.connectedApi = false;
                    if (aqfr.pingTimeout != null) {
                        clearTimeout(aqfr.pingTimeout);
                    }
                };

                aqfr.ws.onmessage = function incoming(data) {
                    const response = JSON.parse(data.data);
                    if (typeof response.id !== 'undefined' && response.id
                        != 'Subscribe to Central') {
                        if (typeof aqfr.requestMap[JSON.parse(
                            data.data).id.toString()]
                            !== 'undefined') {
                            const callback = aqfr.requestMap[JSON.parse(
                                data.data).id.toString()];
                            delete aqfr.requestMap[JSON.parse(
                                data.data).id.toString()];
                            callback(JSON.parse(data.data));
                        }
                    } else {
                        aqfr.subscribeCallback(data.data);
                    }
                };
            } else {
                resolve(aqfr.connectedApi ? 'connected' : 'disconnected');
            }
        });

    }

    getAccountTxns(xrpAddress_, limit) {
        const request = {
            "id": this.id++,
            "command": "account_tx",
            "account": xrpAddress_,
            "ledger_index_min": -1,
            "ledger_index_max": -1,
            "binary": false,
            "limit": limit,
            "forward": false
        };
        const aqfr = this;
        return new Promise(function(resolve, reject) {
            aqfr.requestMap[request.id.toString()] = function (data) {
                resolve(data.result);
            };
            //console.log('--> pay');
            aqfr.ws.send(JSON.stringify(request));
        });

    }

    getAccountTxnsWithMarker(xrpAddress_, limit, marker ) {
        const request = {
            "id": this.id++,
            "command": "account_tx",
            "account": xrpAddress_,
            "ledger_index_min": -1,
            "ledger_index_max": -1,
            "binary": false,
            "limit": limit,
            "forward": false,
            "marker" : marker
        };
        const aqfr = this;
        return new Promise(function(resolve, reject) {
            aqfr.requestMap[request.id.toString()] = function (data) {
                resolve(data.result);
            };
            //console.log('--> pay');
            aqfr.ws.send(JSON.stringify(request));
        });
    }

    toHex(s) {
        // utf8 to latin1
        var s = unescape(encodeURIComponent(s));
        var h = '';
        for (var i = 0; i < s.length; i++) {
            h += s.charCodeAt(i).toString(16);
        }
        return h
    }

    fromHex(h) {
        var s = '';
        for (var i = 0; i < h.length; i += 2) {
            s += String.fromCharCode(parseInt(h.substr(i, 2), 16));
        }
        return decodeURIComponent(escape(s))
    }

    getMode() {
        return this.mode;
    }

    setSequence(sequence, why) {
        this.sequence = sequence;
    }

    async initAccount(options) {
        if (this.xrpAddress == null) {
            if (typeof options.xrpAddress === 'undefined') {
                let data = await this.initTestAccount();
                this.xrpAddress = data.data.account.address;
                this.secret = data.data.account.secret;

                this.setSequence(0, 'init account');

            } else {
                this.xrpAddress = options.xrpAddress;
                this.secret = options.secret;
                const accountInfo = await this.promiseWsAccountInfo(this.xrpAddress);

                this.setSequence(typeof accountInfo.result !== 'undefined' ? accountInfo.result.account_data.Sequence-1: 0, 'init account');
            }
            if (typeof options.keyStore !== 'undefined' && options.keyStore !== null) {
                this.keyStore = options.keyStore;
            }
            if (typeof options.keyStoreListener !== 'undefined' && options.keyStoreListener !== null) {
                this.keyStoreListener = options.keyStoreListener;
            }

        }
        return (this.xrpAddress);
    }

    isValidXrpAddress( address ) {
        return this.api.isValidAddress( address );
    }

    isValidXrpAddressSecret( secret ) {
        return this.api.isValidSecret( secret );
    }

    generateNewXrpAddress() {
        return this.api.generateAddress();
    }

    async initTestAccount() {

        let result = axios.post(
            'https://faucet.altnet.rippletest.net/accounts');
        return Promise.resolve(result);
    }

    getAddress() {
        return this.xrpAddress;
    }
}

exports.Aqfr = Aqfr;
